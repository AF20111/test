import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
//import javax.swing.JOptionPane;

import static javax.swing.JOptionPane.*;

public class SimpleFoodOrderingMachine {
    public JPanel root;
    public JLabel TopLabel1;
    public JButton katudonnButton;
    public JButton oyakodonnButton;
    public JButton gyuudonnButton;
    public JButton kaisenndonnButton;
    public JButton bibinnbaButton;
    public JButton tenndonnButton;
    public JButton checkButton1;
    public JLabel totalamount1;
    public JTextPane receivedinfo1;
    public JLabel Ordereditem1;
    public JButton resetButton1;
    public JTabbedPane Tab;
    public JLabel TopLabel2;
    public JButton uronntyaButton;
    public JButton orennjiButton;
    public JButton koraButton;
    public JButton resetButton2;
    public JButton checkButton2;
    public JButton biruButton;
    public JButton kaferateButton;
    public JButton kohiButton;
    public JTextPane receivedinfo2;
    public JLabel totalamount2;
    public JLabel Ordereditem2;
    public JButton pafeButton;
    public JButton purinnButton;
    public JButton annninndouhuButton;
    public JTextPane receivedinfo3;
    public JLabel totalamount3;
    public JButton resetButton3;
    public JButton checkButton3;
    public JButton syabettoButton;
    public JButton kekiButton;
    public JButton tiramisuButton;
    public JLabel TopLabel3;
    public JLabel Orderditem3;
    public JLabel Label3;
    public JLabel Label2;
    public JLabel Label1;

    int sum = 0;
    int count = 0;

    public SimpleFoodOrderingMachine() {
        //単品
        katudonnButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("pork cutlet bowl", 830);
            }
        });
        katudonnButton.setIcon(new ImageIcon(this.getClass().getResource("katudonn.jpg")
                )
        );

        tenndonnButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("tempura rice bowl", 1000);
            }
        });
        tenndonnButton.setIcon(new ImageIcon(this.getClass().getResource("tenndonn.jpg")
                )
        );

        oyakodonnButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("oyakodon", 710);
            }
        });
        oyakodonnButton.setIcon(new ImageIcon(this.getClass().getResource("oyakodonn.jpeg")
                )
        );

        gyuudonnButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("beef bowl", 750);
            }
        });
        gyuudonnButton.setIcon(new ImageIcon(this.getClass().getResource("gyuudonn.jpg")
                )
        );

        bibinnbaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("bibimbap", 1050);
            }
        });
        bibinnbaButton.setIcon(new ImageIcon(this.getClass().getResource("bibinnba.jpg")
                )
        );

        kaisenndonnButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("seafood don", 1200);
            }
        });
        kaisenndonnButton.setIcon(new ImageIcon(this.getClass().getResource("kaisenndonn.jpg")
                )
        );

        checkButton1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                checkout();
            }
        });
        resetButton1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              reset();
            }
        });

        //ドリンク
        uronntyaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("oolonng tea", 300);
            }
        });
        uronntyaButton.setIcon(new ImageIcon(this.getClass().getResource("u-ronntya.jpg")
                )
        );

        orennjiButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("orange juice", 350);
            }
        });
        orennjiButton.setIcon(new ImageIcon(this.getClass().getResource("orennji.png")
                )
        );

        koraButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("cola", 400);
            }
        });
        koraButton.setIcon(new ImageIcon(this.getClass().getResource("ko-ra.jpg")
                )
        );

        biruButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("beer", 470);
            }
        });
        biruButton.setIcon(new ImageIcon(this.getClass().getResource("bi-ru.jpg")
                )
        );

        kaferateButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("cafe latte", 450);
            }
        });
        kaferateButton.setIcon(new ImageIcon(this.getClass().getResource("kaferate.jpg")
                )
        );

        kohiButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("coffee", 430);
            }
        });
        kohiButton.setIcon(new ImageIcon(this.getClass().getResource("ko-hi-.png")
                )
        );

        checkButton2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                checkout();
            }
        });
        resetButton2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              reset();
            }
        });

        //デザート
        pafeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("parfait", 800);
            }
        });
        pafeButton.setIcon(new ImageIcon(this.getClass().getResource("pafe.jpg")
                )
        );

        purinnButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("purine", 420);
            }
        });
        purinnButton.setIcon(new ImageIcon(this.getClass().getResource("purinn.jpg")
                )
        );

        annninndouhuButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("almond jelly", 500);
            }
        });
        annninndouhuButton.setIcon(new ImageIcon(this.getClass().getResource("annninndouhu.jpg")
                )
        );

        syabettoButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("sherbet", 380);
            }
        });
        syabettoButton.setIcon(new ImageIcon(this.getClass().getResource("sya-betto.jpg")
                )
        );

        kekiButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("cake", 600);
            }
        });
        kekiButton.setIcon(new ImageIcon(this.getClass().getResource("ke-ki.jpg")
                )
        );

        tiramisuButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("tiramisu", 700);
            }
        });
        tiramisuButton.setIcon(new ImageIcon(this.getClass().getResource("tiramisu.jpg")
                )
        );

        checkButton3.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                checkout();
            }
        });
        resetButton3.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                reset();
            }
        });
    }


    void order(String food, int money) {
        int confirmation = JOptionPane.showConfirmDialog(null,
                "Would you like to order " + food + " ?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION);
        if(confirmation == 0){
        ImageIcon icon = new ImageIcon();
        String[] options = {"-1", "+1", "OK", "Cancel"};
        int ret = 0;
        int i = 1;
        while (ret >= 0 && ret < 2) {
            if (ret == 0 && i > 1) {
                i -= 1;
            }
            if (ret == 1) {
                i += 1;
            }
            ret = showOptionDialog(
                    null,
                    "How many do you want?\n" + "Quantity:" + i,
                    "Order Confirmation",
                    DEFAULT_OPTION,
                    INFORMATION_MESSAGE,
                    icon,
                    options,
                    options[0]
            );
        }
            if (ret == 2) {
                String currentText1 = receivedinfo1.getText();
                receivedinfo1.setText(currentText1 + food + " " + money + " yen     X  " + i + "\n");
                String currentText2 = receivedinfo2.getText();
                receivedinfo2.setText(currentText2 + food + " " + money + " yen     X  " + i + "\n");
                String currentText3 = receivedinfo3.getText();
                receivedinfo3.setText(currentText3 + food + " " + money + " yen     X  " + i + "\n");
                sum += money * i;
                totalamount1.setText("Total   " + sum + " yen");
                totalamount2.setText("Total   " + sum + " yen");
                totalamount3.setText("Total   " + sum + " yen");
            }
        }
    }

     void checkout() {
        if (sum == 0) {
            JOptionPane.showConfirmDialog(null,
                    "Not accepting orders",
                    "Message",
                    DEFAULT_OPTION);
        }
        else {
            int confirmation = JOptionPane.showConfirmDialog(null,
                    "Payment of  " + sum + " yen.\n" + "Is it OK?",
                    "Final Confirmation",
                    JOptionPane.YES_NO_OPTION);

            if (confirmation == 0) {
                if(sum >= 2000){
                    sum = (int)(sum * 0.9);
                    totalamount1.setText("Total     " + sum + " yen");
                    totalamount2.setText("Total     " + sum + " yen");
                    totalamount3.setText("Total     " + sum + " yen");
                    JOptionPane.showConfirmDialog(null,
                            "10% discount for purchases over 2000 yen.\n" + "The price will change and become " + sum + " yen.",
                            "Final Confirmation",
                            DEFAULT_OPTION);
                }
                count++;
                JOptionPane.showConfirmDialog(null,
                        "Your number is     " + count + "\n" + "Thank you very much!",
                        "Final Confirmation",
                        DEFAULT_OPTION);
                sum = 0;
                receivedinfo1.setText("");
                totalamount1.setText("Total     " + sum + " yen");
                receivedinfo2.setText("");
                totalamount2.setText("Total     " + sum + " yen");
                receivedinfo3.setText("");
                totalamount3.setText("Total     " + sum + " yen");
            }
        }
    }

     void reset(){
        sum = 0;
        receivedinfo1.setText("");
        totalamount1.setText("Total     " + sum + " yen");
        receivedinfo2.setText("");
        totalamount2.setText("Total     " + sum + " yen");
        receivedinfo3.setText("");
        totalamount3.setText("Total     " + sum + " yen");
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("SimpleFoodOrderingMachine");
        frame.setContentPane(new SimpleFoodOrderingMachine().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}


